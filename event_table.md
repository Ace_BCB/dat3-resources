|                   | Gift-basket | Order | Employee | Product |  Change |
| ----------------- | :----:      | :---: | :------: | :-----: | :-------: |
| Order Order       |             | +     |          |         |           |
| Add Basket        |      +      | *     |          |         |           |
| Remove Basket     |      +      | *     |          |         |           |
| Status changed    |      *      | *     | *        |         |           |
| Order Changed     |             | *     | *        |         |  +        |
| Cancel Order      |             | +     |          |         |           |
| Create Product    |             |       | *        | +       |           |
| Delete Product    |             |       | *        | +       |           |
| Change Product    |             |       | *        | *       | +         |
| Create Employee   |             |       | +        |         |           |
| Delete Employee   |             |       | +        |         |           |
