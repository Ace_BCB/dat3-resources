Til potentiel virksomhed (Isværftet):
- Diskuter forventninger
- Hvor mange timer de/vi ligger i det
- Hvad de forventer af resultat? - ikke nødvendigvis en færdigpakket løsning
- De skal kunne indgå i nogen initierende interviews, løbende spørgsmål og måske evt. test
Der er aftalt ugentlige møder som udgangspunkt om onsdagen, med eventuel brug af Microsoft Teams. Tidspunkt mellem 10-16, det forsøges at ligge dem så tidligt som muligt.