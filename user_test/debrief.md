1. I think that i would like to use this system frequently

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |

2. I found the system unnecessarily complex

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |

3. I thought the system was easy to use

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |

4. I think that i would need the support of a technical person to be able to use this system

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |

5. I thought there was too much inconsistency in this system

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |

6. I would imagine that most people would learn to use this system very quickly

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |

7. I found the system very cumbersome to use

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |

8. I felt very confident using the system

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |

9. I needed to learn a lot of things before i could get going with this system

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |                |
