- likes the look and feel of the site
- would also like to see tha cant follow the page


1. I think that i would like to use this system frequently

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |  x  |                |

2. I found the system unnecessarily complex

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |            x      |     |     |     |                |

3. I thought the system was easy to use

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |            x   |

4. I think that i would need the support of a technical person to be able to use this system

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |     x             |     |     |     |                |

5. I thought there was too much inconsistency in this system

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |             x     |     |     |     |                |

6. I would imagine that most people would learn to use this system very quickly

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |     |    x           |

7. I found the system very cumbersome to use

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |   x |     |     |                |

8. I felt very confident using the system

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |                   |     |     |  x  |                |

9. I needed to learn a lot of things before i could get going with this system

    | Strongly disagree |     |     |     | Strongly agree |
    | :---------------: | :-: | :-: | :-: | :------------: |
    |        x          |     |     |     |                |
