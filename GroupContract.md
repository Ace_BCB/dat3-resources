# Group Contract

## Meeting Times
We meet at 09.00 in the morning unless we have a lucture earlier.

We will leave when we can not be productive any more however, we will aim for 15.00.

Exceptions for meeting times can be made if social life or something other gets in the way, of course the individual will have to notify the group, as early as possible.

## Group Work
Everybody will try to do their best in the project, in the courses it is up to the individual themselves if they want to join in on the group exercises.

## Group Conflict
We will have an intervention and talk about in the group. If this does not work we will bring in the group supervisor or the semester coordinator.

## Supervisor
We expect to meet we our supervisor once a week if possible.

## Programs Used
- **Jira**: For time management and work delegation
- **Discord**: For general communication between group members.
- **Messenger**: Backup communication as some members will notice important messages quicker.
- **Gitlab**: For version control and general file sharing
- **Overleaf**: For writing the project report.
- **Zotero**: For saving sources.

## Programming
We will follow the guidelines from our c# essentials book when we write our code at least the stylistic choices

TODO: Insert these guidelines.
