|                   | Employee | Owner | User | MobilePay |
| ---------------   | :------: | :---: | :--: | :-------: |
| Order Basket      |          |       | x    |           |
| Create Product    | x        | x     |      |           |
| Delete Product    | x        | x     |      |           |
| Add Employee      |          | x     |      |           |
| Remove Employee   |          | x     |      |           |
| Pay Order         |          |       | x    | x         |
| Accept Order      | x        | x     |      |           |
| Decline Order     | x        | x     |      |           |
| Finish Order      | x        | x     |      |           |
| Deliver Order     | x        | x     |      |           |
| Change Deadline   | x        | x     |      |           |
| Cancel Order      | x        | x     | x    |           |
| View History      |          | x     |      |           |
