**Har du nogensinde købt en gavekurv?**

- [ ] Ja
- [ ] Nej
- [ ] Ønsker ikke at svare

**Har du købt eller overvejet at købe en gavekurv fra IsVærftet?**

- [ ] Ja
- [ ] Nej
- [ ] Ønsker ikke at svare

**Hvis du skulle købe en gavekurv, hvor mange penge vil du bruge?**

- [ ] under 100 kr
- [ ] 100 - 200 kr
- [ ] 200 - 300 kr
- [ ] 400 - 500 kr
- [ ] mere end 500 kr

**Hvis det var en mulighed vil du så gerne benytte en hjemmeside til bestilling af gavekurve?**

- [ ] Ja
- [ ] Nej
- [ ] Ønsker ikke at svare

**Hvis du skal bestille noget på nettet, hvilke devices vil du så benytte?**

- [ ] PC
- [ ] Mobil
- [ ] Tablet
- [ ] Andet

**Bestiller du ofte ting på nettet?**

- [ ] En til flere gange om ugen
- [ ] Et par gange om Måneden
- [ ] En gang om Måneden
- [ ] Færre

**Når du køber en gavekurv, er der så noget specifikt du kigger after?**

- [ ] Ja
- [ ] Nej
- [ ] Ønsker ikke at svare

**Kunne det være interessant hvis man kunne vedhæfte en besked når man bestiller, som efterfølgende ville komme på kurven?**

- [ ] Ja
- [ ] Nej
- [ ] Ønsker ikke at svare
