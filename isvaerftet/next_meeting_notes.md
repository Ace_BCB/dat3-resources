# Meeting 01/10
## Open questions
- What kind of inventory system is used, and can we get access to it throught an API?
    - We will not promise this as a feature yet
- How would you like the acceptance of an order to go?
- Show diagrams? + confirm System definition
- Prefrences for webpage design layout, any specific webpage layout (e.g. Pricerunner)
- Status from the guy makeing the webpage

## Specifics
- Should admin users have different Paivalliges?
    - Owner, manager, employee
- Should it be possible to add a comment to both a specific basket, and the order itself?
- Should there be buttons to 'not sort for' or 'not include' haveing specific catagories, when finding a basket.
    - or when finishing the order, clicking of a box to not include specific catagories?
        - Or is this just in the added comment
- Could slider be used to find relevant baskets, or use buttons to sort for price-range?
- Is payment on the website a must have?
- Order cancellation via phone? (Outside our system)
- Customer order cancellation via their mail + a order number

- Would you like to get the questions in advance?

# Meeting 12/10
## Open questions
- Vi vil helst undgå at bruge wordpress. Er det noget vi kan snakke om?
- Hvis der skal bruges wordpress, bliver vi nødt til at vide hvilken package der er (da buissnes giver adgang til plugins, og freelance og private ikke gør)
- Hvilke personfølsomme oplysninger er nødvendige fra kunden? (navn, mail, tlf., adresse etc.)

## Specifics
- Would like empolyees to write their own tags when creating new baskets?
- Would you like to have a digital checklist for orders with many baskets?

# Meeting ??/10
## Open questions
- How would the adding of product to gift-basket work (extra price?)?
- How large of description are you thinking of adding to the gift-basket?


## Specifics
- How often will you be checking for new orders. (If not very often we can update list on refresh instead of using a websocket to update dynamically which is much easier)

